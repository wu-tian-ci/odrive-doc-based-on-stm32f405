#include "foc.h"
#include "foc2.h"
#include "can.h"
#include "hfi.h"
#include "spi.h"
#include "math.h"
#include "arm_math.h"
#include "usbd_cdc_if.h"
#include "tim.h"
#include "tim_6.h"
union
{
	unsigned char u8[2];
	short		  data;
}encoder;


void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  if(htim==&htim6)
	{
		set_motor_pulse(0x21,pulse,pulse2);
	}
}

void set_motor_pulse(char id,short pulse1,short pulse2)
{
	uint32_t send_mail_box;
	CAN_TxHeaderTypeDef  can_tx_message;
	uint8_t  can_send_data[8];
	can_tx_message.StdId=id;
	can_tx_message.IDE=CAN_ID_STD;
	can_tx_message.RTR=CAN_RTR_DATA;
	can_tx_message.DLC=4;
	
	encoder.data = pulse1;
	can_send_data[0] = encoder.u8[0];
	can_send_data[1] = encoder.u8[1];
	
	encoder.data = pulse2;
	can_send_data[2] = encoder.u8[0];
	can_send_data[3] = encoder.u8[1];
	
	HAL_CAN_AddTxMessage(&hcan1, &can_tx_message, can_send_data, &send_mail_box);
}





