#include "math.h"
#include "arm_math.h"
#include "foc.h"
#include "foc2.h"
#include "tim.h"
#include "math.h"
#include "arm_math.h"
/********************************FOC*************************************/
float Ia,Ib,Ic;
float Ialpha,Ibeta;
float Id,Iq;
float Id_last,Iq_last;
float Valpha,Vbeta;
float Vd,Vq;
float theta,THETA,HFI_THETA;
float Ialpha_H,Ibeta_H;

/********************************PID*************************************/
float Iq_p = 0.11f;
float Iq_i = 0.0f;
float Iq_d = 0.0f;

float Id_p = 0.10f;
float Id_i = 0.0f;
float Id_d = 0.0f;

float Speed_p;
float Speed_i;
float Speed_d;

float Pos_p;
float Pos_i;
float Pos_d;

/*******************************SVPWM************************************/
float theta;    
int N;        
float A,B,C;       
float X,Y,Z;    
float Ta,Tb;   
float value1,value2,value3;
int ccr1,ccr2,ccr3; 

/*******************************REPARK***********************************/
void RePark(float vd,float vq)
{
	Valpha = vd * arm_cos_f32(theta) - vq * arm_sin_f32(theta);
	Vbeta = vd * arm_sin_f32(theta) + vq * arm_cos_f32(theta);
}

void ReParkClose(float vd,float vq)
{
	Valpha = vd * arm_cos_f32(THETA) - vq * arm_sin_f32(THETA);
	Vbeta = vd * arm_sin_f32(THETA) + vq * arm_cos_f32(THETA);
}

void TestE_agnleReParkClose(float vd,float vq)
{
	Valpha = vd * arm_cos_f32(0.0f) - vq * arm_sin_f32(0.0f);
	Vbeta = vd * arm_sin_f32(0.0f) + vq * arm_cos_f32(0.0f);
}

void HFI_ReParkClose(float vd,float vq)
{
	Valpha = vd * arm_cos_f32(HFI_THETA) - vq * arm_sin_f32(HFI_THETA);
	Vbeta = vd * arm_sin_f32(HFI_THETA) + vq * arm_cos_f32(HFI_THETA);
}

void HFI_HIGH_ReParkClose(float vd,float vq)
{
	Ialpha_H = vd * arm_cos_f32(HFI_THETA) - vq * arm_sin_f32(HFI_THETA);
	Ibeta_H = vd * arm_sin_f32(HFI_THETA) + vq * arm_cos_f32(HFI_THETA);
}

/********************************PARK***********************************/
void Park(void)
{
	Id = Ialpha*arm_cos_f32(theta) + Ibeta*arm_sin_f32(theta);
	Iq = Ibeta*arm_cos_f32(theta) - Ialpha*arm_sin_f32(theta);
}

void ParkClose(void)
{
	Id = Ialpha*arm_cos_f32(THETA) + Ibeta*arm_sin_f32(THETA);
	Iq = Ibeta*arm_cos_f32(THETA) - Ialpha*arm_sin_f32(THETA);
}

void HFI_ParkClose(void)
{
	Id = Ialpha*arm_cos_f32(HFI_THETA) + Ibeta*arm_sin_f32(HFI_THETA);
	Iq = Ibeta*arm_cos_f32(HFI_THETA) - Ialpha*arm_sin_f32(HFI_THETA);
}

/*******************************CLARK***********************************/
void ClarkClose(void)
{
	Ialpha = Ia;
	Ibeta = TripSqrt3*Ia+2*TripSqrt3*Ib;
}

/*********************************PID***********************************/
void PID_par_init(PID_DEF * pid,float p,float i,float d,float sumerrlimit,float outputlimit)
{
	pid->P = p;
	pid->I = i;
	pid->D = d;
	pid->SUMERRLIMIT = sumerrlimit;
	pid->OUTPUTLIMIT = outputlimit;
}

void PID_init(void)
{
	PID_par_init(&Iq_pid,Iq_p,Iq_i,Iq_d,5.0f,10.0f);
	PID_par_init(&Id_pid,Id_p,Id_i,Id_d,5.0f,10.0f);
	PID_par_init(&speed_pid,0.005f,0.0002f,0.01f,0.1f,30.0f);
	PID_par_init(&pos_pid,0.08f,0.005f,0.0f,100.0f,1000.0f);
	
	PID_par_init(&Iq_pid2,Iq_p2,Iq_i2,Iq_d2,5.0f,10.0f);
	PID_par_init(&Id_pid2,Id_p2,Id_i2,Id_d2,5.0f,10.0f);
	PID_par_init(&speed_pid2,0.005f,0.0002f,0.01f,0.1f,30.0f);
	PID_par_init(&pos_pid2,0.08f,0.005f,0.0f,100.0f,1000.0f);
}

void PID_ABSOLUTE(PID_DEF * pid,float target,float actual)
{
	//目标值
	pid->TARGET = target;
	//实际值
	pid->ACTUAL = actual;
	//误差
	pid->ERR = pid->TARGET - pid->ACTUAL;
	//误差累加
	pid->SUMERR += pid->ERR;
	//误差累加限幅
	if(pid->SUMERR > pid->SUMERRLIMIT) //误差累加大于最大
	{
		pid->SUMERR = pid->SUMERRLIMIT;
	}
	if(pid->SUMERR < -pid->SUMERRLIMIT)//误差累加小于最小
	{
		pid->SUMERR = -pid->SUMERRLIMIT;
	}
	//PID 
	pid->OUTPUT = pid->P*pid->ERR + pid->I*pid->SUMERR + pid->D*(pid->ERR-pid->LASTERR);
	//输出限幅
	if(pid->OUTPUT > pid->OUTPUTLIMIT)
	{
		pid->OUTPUT = pid->OUTPUTLIMIT;
	}
	if(pid->OUTPUT < -pid->OUTPUTLIMIT)
	{
		pid->OUTPUT = -pid->OUTPUTLIMIT;
	}
	pid->LASTERR = pid->ERR;
}

void PID_INCREMENT(PID_DEF * pid,float target,float actual)
{
	pid->TARGET = target;
	pid->ACTUAL = actual;
	pid->ERR = pid->TARGET - pid->ACTUAL;
//	pid->SUMERR += pid->ERR;
//	if(pid->SUMERR > pid->SUMERRLIMIT)
//	{
//		pid->SUMERR = pid->SUMERRLIMIT
//	}
//	if(pid->SUMERR < -pid->SUMERRLIMIT)
//	{
//		pid->SUMERR = -pid->SUMERRLIMIT
//	}
	pid->OUTPUT += (pid->P*(pid->ERR-pid->LASTERR) + pid->I*pid->ERR + pid->D*(pid->LASTERR-pid->LASTLASTERR));
	if(pid->OUTPUT > pid->OUTPUTLIMIT)
	{
		pid->OUTPUT = pid->OUTPUTLIMIT;
	}
	if(pid->OUTPUT < -pid->OUTPUTLIMIT)
	{
		pid->OUTPUT = -pid->OUTPUTLIMIT;
	}
	pid->LASTLASTERR = pid->LASTERR;
	pid->LASTERR = pid->ERR;
}


/***********************************SVPWM***********************************/
void SectorJudge(void)
{
	A = Vbeta;
	B = Valpha*HalfSqrt3 - Vbeta*0.5f;
	C = -Valpha*HalfSqrt3 - Vbeta*0.5f;
	if(A>0.0f)
	{
		N += 1;
	}
	if(B>0.0f)
	{
		N += 2;
	}
	if(C>0.0f)
	{
		N += 4;
	}
}

void VectorTime(void)
{
	float M = (Sqrt3*Tpwm)/Udc;
	X = M*A;
	Y = M*B;
	Z = M*C;
	if(N==3)
	{
		Ta = Y;
		Tb = X;
	}
	else if(N==1)
	{
		Ta = -Y;
		Tb = -Z;
	}
	else if(N==5)
	{
		Ta = X;
		Tb = Z;
	}
	else if(N==4)
	{
		Ta = -X;
		Tb = -Y;
	}
	else if(N==6)
	{
		Ta = Z;
		Tb = Y;
	}
	else if(N==2)
	{
		Ta = -Z;
		Tb = -X;
	}
}

void CCRcalculate(void)
{
	float temp = Ta + Tb;
	if(temp>Tpwm)
	{
		Ta = Ta/temp*Tpwm;
		Tb = Tb/temp*Tpwm;
	}
	value1 = (Tpwm - Ta - Tb)*0.25f;
	value2 = value1 + Ta*0.5f;
	value3 = value2 + Tb*0.5f;
	switch(N)
	{
		case 3:
			ccr1 = (int)value1;
			ccr2 = (int)value2;
			ccr3 = (int)value3;
			break;
		case 1:
			ccr1 = (int)value2;
			ccr2 = (int)value1;
			ccr3 = (int)value3;
			break;
		case 5:
			ccr1 = (int)value3;
			ccr2 = (int)value1;
			ccr3 = (int)value2;
			break;
		case 4:
			ccr1 = (int)value3;
			ccr2 = (int)value2;
			ccr3 = (int)value1;
			break;
		case 6:
			ccr1 = (int)value2;
			ccr2 = (int)value3;
			ccr3 = (int)value1;
			break;
		case 2:
			ccr1 = (int)value1;
			ccr2 = (int)value3;
			ccr3 = (int)value2;
			break;
		default:
			break;
	}
}

void FOCdataClear(void)
{
	N = 0;
}

void StartStopMotor(int CMD)
{
	if(CMD == 0)
	{
		TIM1->CCR1 = 0;
		TIM1->CCR2 = 0;
		TIM1->CCR3 = 0;
	}
	if(CMD == 1)
	{
		TIM1->CCR1 = ccr1;
		TIM1->CCR2 = ccr2;
		TIM1->CCR3 = ccr3;
	}
}

PID_DEF Id_pid;
PID_DEF Iq_pid;
PID_DEF speed_pid;
PID_DEF pos_pid;

PID_DEF Id_pid2;
PID_DEF Iq_pid2;
PID_DEF speed_pid2;
PID_DEF pos_pid2;



