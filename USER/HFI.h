#ifndef __HFI_H__
#define __HFI_H__

/*高频注入频率*/
#define HFI_500HZ_CNT 20000/500  //10000为电流环频率 500为注入频率
#define HFI_PERIOD 0.00005f       //电流环频率



typedef struct
{
	float states0;
	float states1;
	float b0;
	float b1;
	float b2;
	float a0;
	float a1;
	float a2;
	float gain0;
	float gain1;
}IIR_BUTTERWORTH_DEF;


extern IIR_BUTTERWORTH_DEF D_IIR_LPF_Par;
extern IIR_BUTTERWORTH_DEF Q_IIR_LPF_Par;
extern IIR_BUTTERWORTH_DEF Q_IIR_BPF_Par;
extern IIR_BUTTERWORTH_DEF ERR_IIR_LPF_Par;
extern IIR_BUTTERWORTH_DEF SPEED_LPF_Par;
extern IIR_BUTTERWORTH_DEF SPEED_LPF_Par2;
extern IIR_BUTTERWORTH_DEF WE_LPF_Par;
extern IIR_BUTTERWORTH_DEF Q_IIR_HPF_Par;
extern IIR_BUTTERWORTH_DEF HFI_THETA_LPF_Par;
extern IIR_BUTTERWORTH_DEF Ia_LPF_Par;
extern IIR_BUTTERWORTH_DEF Ib_LPF_Par;
extern IIR_BUTTERWORTH_DEF IQ_HPF_Par;
extern IIR_BUTTERWORTH_DEF ID_HPF_Par;


extern float IIR_BUTTERWORTH_out;
extern float IIR_BUTTERWORTH_BPF_out;
/*****************高频注入*******************/
extern float HFI_cos;
extern float HFI_sin;
extern float HFI_inj_volt;
extern float HFI_inj_volt_amp_temp;
extern int HFI_inj_volt_ok;
extern int HFI_inj_volt_amp_ok_cnt;
extern float IQ_BPF_SIN;
extern float IQ_BPF_SIN_LPF;
extern float HFI_WE;
extern float HFI_THETA;
extern float HFI_REAL_THETA;
extern float Iq_H;
extern float Id_H;
extern float Iq_HH;
extern float Id_HH;
/******************HFI pid*******************/
extern float HFI_P;
extern float HFI_I;
extern float HFI_Err;
extern float HFI_SumErr;
extern float HFI_Out;
extern float HFI_SumErr_MIN;
extern float HFI_SumErr_MAX;
extern float HFI_MIN_OUT;
extern float HFI_MAX_OUT;

extern float HFI_Integrator;

extern float HFI_PID_OUT;
extern float HFI_PID_P;
extern float HFI_PID_I;
extern float HFI_PID_I_SUM;
/********************************************/
float IIR_Butterworth_Iq(float in);
float IIR_Butterworth_Id(float in);
float IIR_Butterworth_IQ_BPF_SIN_LPF(float in);
float IIR_Butterworth_BPF(float in);
void HFI_Input(void);
void HFI_PID(float p,float i,float MIN_SumErr,float MAX_SumErr,float MIN_OUT,float MAX_OUT);
void D_direction_judge(float in_temp);
void segger_init(void);


void IIR_Init(void);
void IIR_Butterworth_Coefficient_Init(float temp[8],IIR_BUTTERWORTH_DEF* iir_butterworth_temp);
void IIR_Butterworth(float in,float* out,IIR_BUTTERWORTH_DEF* iir_butterworth_temp);

#endif
