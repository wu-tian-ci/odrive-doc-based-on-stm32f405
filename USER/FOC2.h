#ifndef __FOC2_H__
#define __FOC2_H__

/********************************FOC*************************************/
extern float Ia2,Ib2,Ic2;
extern float Ialpha2,Ibeta2;
extern float Id2,Iq2;
extern float Id_last2,Iq_last2;
extern float Valpha2,Vbeta2;
extern float Vd2,Vq2;
extern float theta2,THETA2,HFI_THETA2;
extern float Ialpha_H2,Ibeta_H2;

/********************************PID*************************************/
extern float Iq_p2;
extern float Iq_i2;
extern float Iq_d2;

extern float Id_p2;
extern float Id_i2;
extern float Id_d2;

extern float Speed_p2;
extern float Speed_i2;
extern float Speed_d2;

extern float Pos_p2;
extern float Pos_i2;
extern float Pos_d2;

/*******************************SVPWM************************************/
extern float theta2;    
extern int N2;        
extern float A2,B2,C2;       
extern float X2,Y2,Z2;    
extern float Ta2,Tb2;   
extern float value1_2,value2_2,value3_2;
extern int ccr1_2,ccr2_2,ccr3_2; 


void RePark2(float vd2,float vq2);
void ReParkClose2(float vd2,float vq2);
void TestE_agnleReParkClose2(float vd2,float vq2);
void Park2(void);
void ParkClose2(void);
void ClarkClose2(void);
void SectorJudge2(void);
void VectorTime2(void);
void CCRcalculate2(void);
void FOCdataClear2(void);
void StartStopMotor2(int CMD);


#endif
