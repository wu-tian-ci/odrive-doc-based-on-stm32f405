#include "math.h"
#include "arm_math.h"
#include "foc.h"
#include "tim.h"
#include "math.h"
#include "arm_math.h"
#include "HFI.h"
#include "FOC.h"
/*****************高频注入*******************/
int HFI_cnt = 0;
float HFI_cos;
float HFI_sin;
float HFI_inj_volt;                //注入电压
float HFI_inj_volt_amp = 1.0f;     //注入电压幅值
int HFI_inj_volt_ok;
int HFI_inj_volt_amp_ok_cnt;
int D_direction_ok;
int D_direction_ok_cnt1;
int D_direction_ok_cnt2;
int id_index;
float id_max;
float id_min;
float HFI_inj_volt_amp_temp;
int out_temp;
float Iq_H;
float Id_H;
float Iq_HH;
float Id_HH;
float IQ_BPF_SIN;                  //滤出的高频iq电流与sin值相乘的值 就是角度误差
float IQ_BPF_SIN_LPF;              //对滤出的高频iq电流与sin值相乘的值的低通滤波
float HFI_WE;                      //角速度
extern float HFI_THETA;            //角度
float HFI_REAL_THETA;
/******************HFI pid*******************/
float HFI_P = 0.0f;
float HFI_I = 0.000f;
float HFI_Err;
float HFI_SumErr;
float HFI_Out;
float HFI_SumErr_MIN = 3000.0f;
float HFI_SumErr_MAX = -3000.0f;
float HFI_MIN_OUT = 5000.0f;
float HFI_MAX_OUT = -5000.0f;

float HFI_Integrator;              //pid输出量积分值


float HFI_PID_OUT;
float HFI_PID_P = 500.0f;
float HFI_PID_I = 0.0f;
float HFI_PID_I_SUM;
/********************************************/

void IIR_Butterworth_Coefficient_Init(float temp[8],IIR_BUTTERWORTH_DEF* iir_butterworth_temp)
{
  iir_butterworth_temp->b0 = temp[0];
  iir_butterworth_temp->b1 = temp[1];
  iir_butterworth_temp->b2 = temp[2];
  iir_butterworth_temp->a0 = temp[3];
  iir_butterworth_temp->a1 = temp[4];
  iir_butterworth_temp->a2 = temp[5];
  iir_butterworth_temp->gain0 = temp[6];
  iir_butterworth_temp->gain1 = temp[7];
  iir_butterworth_temp->states0 = 0.0f;
  iir_butterworth_temp->states1 = 0.0f;
}
void IIR_Butterworth(float in,float* out,IIR_BUTTERWORTH_DEF* iir_butterworth_temp)
{
  float temp;
  temp =(iir_butterworth_temp->gain0 * in
         -iir_butterworth_temp->a1 * iir_butterworth_temp->states0)
    -iir_butterworth_temp->a2 * iir_butterworth_temp->states1;
  *out = ((iir_butterworth_temp->b0 * temp + iir_butterworth_temp->b1 * iir_butterworth_temp->states0) +
          iir_butterworth_temp->b2 * iir_butterworth_temp->states1)*iir_butterworth_temp->gain1;
  
  iir_butterworth_temp->states1 = iir_butterworth_temp->states0;
  iir_butterworth_temp->states0 = temp;
}


IIR_BUTTERWORTH_DEF D_IIR_LPF_Par;
IIR_BUTTERWORTH_DEF Q_IIR_LPF_Par;
IIR_BUTTERWORTH_DEF Q_IIR_BPF_Par;
IIR_BUTTERWORTH_DEF ERR_IIR_LPF_Par;
IIR_BUTTERWORTH_DEF SPEED_LPF_Par;
IIR_BUTTERWORTH_DEF SPEED_LPF_Par2;
IIR_BUTTERWORTH_DEF WE_LPF_Par;
IIR_BUTTERWORTH_DEF SPEED_RPM;
IIR_BUTTERWORTH_DEF Q_IIR_HPF_Par;
IIR_BUTTERWORTH_DEF HFI_THETA_LPF_Par;
IIR_BUTTERWORTH_DEF Ia_LPF_Par;
IIR_BUTTERWORTH_DEF Ib_LPF_Par;
IIR_BUTTERWORTH_DEF IQ_HPF_Par;
IIR_BUTTERWORTH_DEF ID_HPF_Par;

void IIR_Init(void)
{
	float D_IIR_LPF_Coeff[8]={1.0f,1.0f,0.0f,1.0f,-0.9844141274f,0.0f,0.007792936292f,1.0f};
  float Q_IIR_LPF_Coeff[8]={1.0f,1.0f,0.0f,1.0f,-0.9844141274f,0.0f,0.007792936292f,1.0f};
  float Q_IIR_BPF_Coeff[8]={1.0f,0.0f,-1.0f,1.0f,-1.9879266250f,0.9921767001f,0.0039116499f,1.0f};
  float ERR_IIR_LPF_Coeff[8]={1.0f,1.0f,0.0f,1.0f,-0.9844141274f,0.0f,0.007792936292f,1.0f};
	float SPEED_LPF_Coeff[8]={1.0f,1.0f,0.0f,1.0f,-0.9844141274f,0.0f,0.007792936292f,1.0f};
	float SPEED_LPF_Coeff2[8]={1.0f,1.0f,0.0f,1.0f,-0.9844141274f,0.0f,0.007792936292f,1.0f};
	float WE_LPF_Par_Coeff[8]={1.0f,1.0f,0.0f,1.0f,-0.9844141274f,0.0f,0.007792936292f,1.0f};
	float Q_IIR_HPF_Coeff[8]={1.0f,-1.0f,0.0f,1.0f,-1.9955571f,0.0f,0.5000000000000f,1.0f};
	float HFI_THETA_LPF_Coeff[8]={1.0f,2.0f,1.0f,1.0f,-0.9968633318f, 0.9955669720f,0.00000246193004f,1.0f};
	float Ia_LPF_Coeff[8]={1.0f,1.0f,0.0f,1.0f,-0.9844141274f,0.0f,0.007792936292f,1.0f};
	float Ib_LPF_Coeff[8]={1.0f,1.0f,0.0f,1.0f,-0.9844141274f,0.0f,0.007792936292f,1.0f};
	float IQ_HPF_Coeff[8]={1.0f,-1.0f,0.0f,1.0f,-0.000000000000000f,0.0f,0.5000000000000f,1.0f};
	float ID_HPF_Coeff[8]={1.0f,-1.0f,0.0f,1.0f,-0.000000000000000f,0.0f,0.5000000000000f,1.0f};
	
  IIR_Butterworth_Coefficient_Init(D_IIR_LPF_Coeff,&D_IIR_LPF_Par);	
  IIR_Butterworth_Coefficient_Init(Q_IIR_LPF_Coeff,&Q_IIR_LPF_Par);	
  IIR_Butterworth_Coefficient_Init(Q_IIR_BPF_Coeff,&Q_IIR_BPF_Par);	
  IIR_Butterworth_Coefficient_Init(ERR_IIR_LPF_Coeff,&ERR_IIR_LPF_Par);
	IIR_Butterworth_Coefficient_Init(SPEED_LPF_Coeff,&SPEED_LPF_Par);
	IIR_Butterworth_Coefficient_Init(SPEED_LPF_Coeff2,&SPEED_LPF_Par2);
	IIR_Butterworth_Coefficient_Init(WE_LPF_Par_Coeff,&WE_LPF_Par);
	IIR_Butterworth_Coefficient_Init(Q_IIR_HPF_Coeff,&Q_IIR_HPF_Par);
	IIR_Butterworth_Coefficient_Init(HFI_THETA_LPF_Coeff,&HFI_THETA_LPF_Par);
	IIR_Butterworth_Coefficient_Init(Ia_LPF_Coeff,&Ia_LPF_Par);
	IIR_Butterworth_Coefficient_Init(Ib_LPF_Coeff,&Ib_LPF_Par);
	IIR_Butterworth_Coefficient_Init(IQ_HPF_Coeff,&IQ_HPF_Par);
	IIR_Butterworth_Coefficient_Init(ID_HPF_Coeff,&ID_HPF_Par);
}

void HFI_Input(void)                //高频电压注入函数
{
	if(HFI_cnt >= HFI_500HZ_CNT)
	{
		HFI_cnt = 0;
	}
	HFI_cos = arm_cos_f32((float)(HFI_cnt)/(float)(HFI_500HZ_CNT)*2.0f*PI);
	HFI_sin = arm_sin_f32((float)(HFI_cnt)/(float)(HFI_500HZ_CNT)*2.0f*PI);
	if(HFI_inj_volt_ok == 0)
	{
		if(HFI_inj_volt_amp_temp<HFI_inj_volt_amp)
		{
			HFI_inj_volt_amp_temp += 0.001f;	
		}
		else
		{
			if(HFI_inj_volt_amp_ok_cnt>=1000)
			{
				HFI_inj_volt_ok = 1;
			}
			else
			{
			HFI_inj_volt_amp_ok_cnt ++;
			}
		}	
	}
//	HFI_inj_volt = HFI_cos * HFI_inj_volt_amp_temp;  //使用D轴判断
	HFI_inj_volt = HFI_cos * HFI_inj_volt_amp;  //不使用D轴判断
	
	HFI_cnt++;
}

void D_direction_judge(float in_temp)
{
	if(HFI_inj_volt_ok == 1)
	{
		if(D_direction_ok == 0)
		{
			if(id_index<20)
			{
				if(in_temp>id_max)
				{
					id_max = in_temp;
				}
				if(in_temp<id_max)
				{
					id_min = in_temp;
				}
				id_index++;
			}
			else
			{
				if(id_max > (-id_min))
				{
					out_temp = 1;
					if(D_direction_ok_cnt1>20)
					{
						D_direction_ok = 1;
					}
					D_direction_ok_cnt1 ++;
				}
				else
				{
					if(D_direction_ok_cnt2>200)
					{
						HFI_Integrator = HFI_Integrator + PI;
						D_direction_ok_cnt2 = 0;
					}
					D_direction_ok_cnt2 ++;
					out_temp = 2;
				}
				id_max = 0.0f;
				id_min = 0.0f;
				id_index = 0;
			}
		}
	}
}


