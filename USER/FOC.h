#ifndef __FOC_H__
#define __FOC_H__


#define Udc 12     
#define Tpwm 8400  


#define Sqrt3      1.7320508f
#define HalfSqrt3  0.8660254f
#define TripSqrt3  0.5773503f

extern float theta;    
extern int N;        
extern float A,B,C;       
extern float X,Y,Z;    
extern float Ta,Tb;   
extern float value1,value2,value3;
extern int ccr1,ccr2,ccr3;    


extern float Ia,Ib,Ic;
extern float Ialpha,Ibeta;
extern float Id,Iq;
extern float Id_last,Iq_last;
extern float Valpha,Vbeta;
extern float Vd,Vq;
extern float theta,THETA,HFI_THETA;
extern float Ialpha_H,Ibeta_H;

extern float Iq_p;
extern float Iq_i;
extern float Iq_d;

extern float Id_p;
extern float Id_i;
extern float Id_d;

extern float Speed_p;
extern float Speed_i;
extern float Speed_d;

extern float Pos_p;
extern float Pos_i;
extern float Pos_d;

typedef struct
{
	float P;
	float I;
	float D;
	float ACTUAL;
	float TARGET;
	float ERR;
	float SUMERR;
	float LASTERR;
	float LASTLASTERR;
	float OUTPUT;
	float SUMERRLIMIT;
	float OUTPUTLIMIT;
}PID_DEF;

extern PID_DEF Id_pid;
extern PID_DEF Iq_pid;
extern PID_DEF speed_pid;
extern PID_DEF pos_pid;

extern PID_DEF Id_pid2;
extern PID_DEF Iq_pid2;
extern PID_DEF speed_pid2;
extern PID_DEF pos_pid2;

void RePark(float vd,float vq);
void ReParkClose(float vd,float vq);
void TestE_agnleReParkClose(float vd,float vq);
void HFI_ReParkClose(float vd,float vq);
void HFI_HIGH_ReParkClose(float vd,float vq);

void Park(void);
void ParkClose(void);
void HFI_ParkClose(void);
void ClarkClose(void);

void PID_par_init(PID_DEF * pid,float p,float i,float d,float sumerrlimit,float outputlimit);
void PID_init(void);
void PID_ABSOLUTE(PID_DEF * pid,float target,float actual);
void PID_INCREMENT(PID_DEF * pid,float target,float actual);

void SectorJudge(void);
void VectorTime(void);
void CCRcalculate(void);
void FOCdataClear(void);
void StartStopMotor(int CMD);

#endif
