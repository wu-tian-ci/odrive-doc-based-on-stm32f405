#include "math.h"
#include "arm_math.h"
#include "foc2.h"
#include "foc.h"
#include "tim.h"
#include "math.h"
#include "arm_math.h"
/********************************FOC*************************************/
float Ia2,Ib2,Ic2;
float Ialpha2,Ibeta2;
float Id2,Iq2;
float Id_last2,Iq_last2;
float Valpha2,Vbeta2;
float Vd2,Vq2;
float theta2,THETA2,HFI_THETA2;
float Ialpha_H2,Ibeta_H2;

/********************************PID*************************************/
float Iq_p2 = 0.11f;
float Iq_i2 = 0.0f;
float Iq_d2 = 0.0f;

float Id_p2 = 0.10f;
float Id_i2 = 0.0f;
float Id_d2 = 0.0f;

float Speed_p2;
float Speed_i2;
float Speed_d2;

float Pos_p2;
float Pos_i2;
float Pos_d2;

/*******************************SVPWM************************************/
float theta2;    
int N2;        
float A2,B2,C2;       
float X2,Y2,Z2;    
float Ta2,Tb2;   
float value1_2,value2_2,value3_2;
int ccr1_2,ccr2_2,ccr3_2; 

/*******************************REPARK***********************************/
void RePark2(float vd2,float vq2)
{
	Valpha2 = vd2 * arm_cos_f32(theta2) - vq2 * arm_sin_f32(theta2);
	Vbeta2 = vd2 * arm_sin_f32(theta2) + vq2 * arm_cos_f32(theta2);
}

void ReParkClose2(float vd2,float vq2)
{
	Valpha2 = vd2 * arm_cos_f32(THETA2) - vq2 * arm_sin_f32(THETA2);
	Vbeta2 = vd2 * arm_sin_f32(THETA2) + vq2 * arm_cos_f32(THETA2);
}

void TestE_agnleReParkClose2(float vd2,float vq2)
{
	Valpha2 = vd2 * arm_cos_f32(0.0f) - vq2 * arm_sin_f32(0.0f);
	Vbeta2 = vd2 * arm_sin_f32(0.0f) + vq2 * arm_cos_f32(0.0f);
}

/********************************PARK***********************************/
void Park2(void)
{
	Id2 = Ialpha2*arm_cos_f32(theta2) + Ibeta2*arm_sin_f32(theta2);
	Iq2 = Ibeta2*arm_cos_f32(theta2) - Ialpha2*arm_sin_f32(theta2);
}

void ParkClose2(void)
{
	Id2 = Ialpha2*arm_cos_f32(THETA2) + Ibeta2*arm_sin_f32(THETA2);
	Iq2 = Ibeta2*arm_cos_f32(THETA2) - Ialpha2*arm_sin_f32(THETA2);
}

/*******************************CLARK***********************************/
void ClarkClose2(void)
{
	Ialpha2 = Ia2;
	Ibeta2 = TripSqrt3*Ia2+2*TripSqrt3*Ib2;
}

/***********************************SVPWM***********************************/
void SectorJudge2(void)
{
	A2 = Vbeta2;
	B2 = Valpha2*HalfSqrt3 - Vbeta2*0.5f;
	C2 = -Valpha2*HalfSqrt3 - Vbeta2*0.5f;
	if(A2>0.0f)
	{
		N2 += 1;
	}
	if(B2>0.0f)
	{
		N2 += 2;
	}
	if(C2>0.0f)
	{
		N2 += 4;
	}
}

void VectorTime2(void)
{
	float M2 = (Sqrt3*Tpwm)/Udc;
	X2 = M2*A2;
	Y2 = M2*B2;
	Z2 = M2*C2;
	if(N2==3)
	{
		Ta2 = Y2;
		Tb2 = X2;
	}
	else if(N2==1)
	{
		Ta2 = -Y2;
		Tb2 = -Z2;
	}
	else if(N2==5)
	{
		Ta2 = X2;
		Tb2 = Z2;
	}
	else if(N2==4)
	{
		Ta2 = -X2;
		Tb2 = -Y2;
	}
	else if(N2==6)
	{
		Ta2 = Z2;
		Tb2 = Y2;
	}
	else if(N2==2)
	{
		Ta2 = -Z2;
		Tb2 = -X2;
	}
}

void CCRcalculate2(void)
{
	float temp2 = Ta2 + Tb2;
	if(temp2>Tpwm)
	{
		Ta2 = Ta2/temp2*Tpwm;
		Tb2 = Tb2/temp2*Tpwm;
	}
	value1_2 = (Tpwm - Ta2 - Tb2)*0.25f;
	value2_2 = value1_2 + Ta2*0.5f;
	value3_2 = value2_2 + Tb2*0.5f;
	switch(N2)
	{
		case 3:
			ccr1_2 = (int)value1_2;
			ccr2_2 = (int)value2_2;
			ccr3_2 = (int)value3_2;
			break;
		case 1:
			ccr1_2 = (int)value2_2;
			ccr2_2 = (int)value1_2;
			ccr3_2 = (int)value3_2;
			break;
		case 5:
			ccr1_2 = (int)value3_2;
			ccr2_2 = (int)value1_2;
			ccr3_2 = (int)value2_2;
			break;
		case 4:
			ccr1_2 = (int)value3_2;
			ccr2_2 = (int)value2_2;
			ccr3_2 = (int)value1_2;
			break;
		case 6:
			ccr1_2 = (int)value2_2;
			ccr2_2 = (int)value3_2;
			ccr3_2 = (int)value1_2;
			break;
		case 2:
			ccr1_2 = (int)value1_2;
			ccr2_2 = (int)value3_2;
			ccr3_2 = (int)value2_2;
			break;
		default:
			break;
	}
}

void FOCdataClear2(void)
{
	N2 = 0;
}

void StartStopMotor2(int CMD)
{
	if(CMD == 0)
	{
		TIM8->CCR1 = 0;
		TIM8->CCR2 = 0;
		TIM8->CCR3 = 0;
	}
	if(CMD == 1)
	{
		TIM8->CCR1 = ccr1_2;
		TIM8->CCR2 = ccr2_2;
		TIM8->CCR3 = ccr3_2;
	}
}



