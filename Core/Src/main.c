/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "can.h"
#include "spi.h"
#include "tim.h"
#include "usart.h"
#include "usb_device.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include "usbd_cdc_if.h"
#include "math.h"
#include "arm_math.h"
#include "foc.h"
#include "foc2.h"
#include "HFI.h"
#include "tim_6.h"
#include "lcd.h"
#include "lcd_init.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
int test_a;
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
  {
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
	LCD_Init();
	LCD_Fill(0,0,LCD_W,LCD_H,WHITE);
  MX_ADC1_Init();
  MX_CAN1_Init();
  MX_SPI3_Init();
  MX_TIM1_Init();
  MX_UART4_Init();
  MX_USB_DEVICE_Init();
  MX_TIM8_Init();
  MX_ADC2_Init();
  MX_TIM6_Init();
  /* USER CODE BEGIN 2 */
	drv_register_control();
	drv_register_control2();
	IIR_Init();
	PID_init();
	HAL_TIM_PWM_Start(&htim1,TIM_CHANNEL_1);
	HAL_TIM_PWM_Start(&htim1,TIM_CHANNEL_2);
	HAL_TIM_PWM_Start(&htim1,TIM_CHANNEL_3);
	HAL_TIM_PWM_Start(&htim1,TIM_CHANNEL_4);
	HAL_TIMEx_PWMN_Start(&htim1,TIM_CHANNEL_1);
	HAL_TIMEx_PWMN_Start(&htim1,TIM_CHANNEL_2);
	HAL_TIMEx_PWMN_Start(&htim1,TIM_CHANNEL_3);

	HAL_TIM_PWM_Start(&htim8,TIM_CHANNEL_1);
	HAL_TIM_PWM_Start(&htim8,TIM_CHANNEL_2);
	HAL_TIM_PWM_Start(&htim8,TIM_CHANNEL_3);
	HAL_TIM_PWM_Start(&htim8,TIM_CHANNEL_4);
	HAL_TIMEx_PWMN_Start(&htim8,TIM_CHANNEL_1);
	HAL_TIMEx_PWMN_Start(&htim8,TIM_CHANNEL_2);
	HAL_TIMEx_PWMN_Start(&htim8,TIM_CHANNEL_3);

	HAL_Delay(500);
	CAN_Filter_Config();
	HAL_CAN_Start(&hcan1);
	HAL_CAN_ActivateNotification(&hcan1, CAN_IT_RX_FIFO0_MSG_PENDING);
	
	HAL_ADC_Start_IT(&hadc1);
	HAL_ADCEx_InjectedStart_IT(&hadc1);
	HAL_ADCEx_InjectedStart_IT(&hadc2);
	
	HAL_TIM_Base_Start_IT(&htim6);

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
		pulse = A5047_Read();
		pulse_to_THETA(600,2900,5280,7600,9930,12280,14650);
		pulse2 = A5047_Read_2();
		pulse_to_THETA_2(600,2900,5280,7600,9930,12280,14650);
		
	
//		usbvcom_printf("%d,%d\n",pulse,pulse2);
//		LCD_ShowFloatNum1(120,40,pulse,6,RED,BLACK,24);
		
//		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_2, GPIO_PIN_SET);
//		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3, GPIO_PIN_RESET);
//		PID_init();

//		usbvcom_printf("%f\n",(float)(spi_encoder_data1));
//		usbvcom_printf("%f,%f\n",(float)(spi_encoder_data1),(float)(spi_encoder_data1));
//		printf("%d\n",test_a);
//		printf("%d\n",TIM1->CNT);
//		printf("1\n");
		
//		usbvcom_printf("%d,%d,%d,%d\n",adc_data1,adc_data2,adc_data3,adc_data4);
//		usbvcom_printf("%d\n",adc_data4);

//		usbvcom_printf("%d,%d\n",adc_data1,adc_data2);
//		usbvcom_printf("%d,%d\n",adc_data2,adc_data3);
//		usbvcom_printf("%f\n",arm_cos_f32(3.14f));
//		usbvcom_printf("%d,%d,%d\n",ccr1,ccr2,ccr3);
//		adc_data6 = 6144 - adc_data3 - adc_data4;
//		usbvcom_printf("%d,%d,%d\n",adc_data3,adc_data4,adc_data6);
//		usbvcom_printf("%d,%d,%d\n",ccr1_2,ccr2_2,ccr3_2);

//		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_2, GPIO_PIN_RESET);
//		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3, GPIO_PIN_RESET);
//		Ic = -Ia - Ib;
//		usbvcom_printf("%f,%f\n",Ia,Ib);
//		Ic2 = -Ia2 - Ib2;
//		usbvcom_printf("%f,%f,%f\n",Ia2,Ib2,Ic2);

//		usbvcom_printf("%f,%f\n",current1,current2);
//		usbvcom_printf("%f,%f\n",THETA);
//		usbvcom_printf("%f,%f\n",THETA2);
//		usbvcom_printf("%d,%d\n",pulse,pulse2);
//		usbvcom_printf("%d,%d\n",adc_data1,adc_data2);
//		usbvcom_printf("%f,%f,%f\n",Ia,Ib,Ic);
//		usbvcom_printf("%f,%f\n",Ialpha,Ibeta);
//		usbvcom_printf("%f,%f\n",Iq_pid.OUTPUT,Id_pid.OUTPUT);
//		usbvcom_printf("%f,%f\n",speed_rpm,speed_rpm2);
//		usbvcom_printf("%f,%f\n",Id,Iq);
//		usbvcom_printf("%f,%f,%f,%f\n",Id,Iq,Id_pid.OUTPUT,Iq_pid.OUTPUT);
//		usbvcom_printf("%f,%f\n",THETA,HFI_WE);
//		usbvcom_printf("%f,%f\n",THETA,HFI_THETA);
//		usbvcom_printf("%f\n",Iq_HH);
//		usbvcom_printf("%f\n",Iq_H);
//		usbvcom_printf("%f\n",IQ_BPF_SIN);
//		usbvcom_printf("%f\n",IQ_BPF_SIN_LPF);
//		usbvcom_printf("%f\n",cos(0.0f));
//		usbvcom_printf("%d\n",spi_encoder_data1);
//		usbvcom_printf("%f,%f,%f\n",A,B,C);
//		usbvcom_printf("%d\n",test_a);
//		HAL_Delay(1000);
		
/******************************************************************************************************************************/	
	
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 336;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 7;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
