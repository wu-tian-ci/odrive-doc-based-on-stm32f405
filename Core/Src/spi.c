/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file    spi.c
  * @brief   This file provides code for the configuration
  *          of the SPI instances.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "spi.h"

/* USER CODE BEGIN 0 */
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include "usbd_cdc_if.h"
#include "can.h"
#include "foc.h"
#include "foc2.h"

float angle;
float e_angle;
int lastpulse,deltapulse;
long long totalpulse;
long long lasttotalpulse;
long long speed_totalpulse;
float speed_rpm,speed_rps;

float angle2;
float e_angle2;
int lastpulse2,deltapulse2;
long long totalpulse2;
long long lasttotalpulse2;
long long speed_totalpulse2;
float speed_rpm2,speed_rps2;

/* USER CODE END 0 */

SPI_HandleTypeDef hspi3;

/* SPI3 init function */
void MX_SPI3_Init(void)
{

  /* USER CODE BEGIN SPI3_Init 0 */

  /* USER CODE END SPI3_Init 0 */

  /* USER CODE BEGIN SPI3_Init 1 */

  /* USER CODE END SPI3_Init 1 */
  hspi3.Instance = SPI3;
  hspi3.Init.Mode = SPI_MODE_MASTER;
  hspi3.Init.Direction = SPI_DIRECTION_2LINES;
  hspi3.Init.DataSize = SPI_DATASIZE_16BIT;
  hspi3.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi3.Init.CLKPhase = SPI_PHASE_2EDGE;
  hspi3.Init.NSS = SPI_NSS_SOFT;
  hspi3.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_256;
  hspi3.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi3.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi3.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi3.Init.CRCPolynomial = 10;
  if (HAL_SPI_Init(&hspi3) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI3_Init 2 */
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12, GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_2, GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3, GPIO_PIN_SET);
  /* USER CODE END SPI3_Init 2 */

}

void HAL_SPI_MspInit(SPI_HandleTypeDef* spiHandle)
{

  GPIO_InitTypeDef GPIO_InitStruct = {0};
  if(spiHandle->Instance==SPI3)
  {
  /* USER CODE BEGIN SPI3_MspInit 0 */

  /* USER CODE END SPI3_MspInit 0 */
    /* SPI3 clock enable */
    __HAL_RCC_SPI3_CLK_ENABLE();

    __HAL_RCC_GPIOC_CLK_ENABLE();
    /**SPI3 GPIO Configuration
    PC10     ------> SPI3_SCK
    PC11     ------> SPI3_MISO
    PC12     ------> SPI3_MOSI
    */
    GPIO_InitStruct.Pin = GPIO_PIN_10|GPIO_PIN_11|GPIO_PIN_12;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF6_SPI3;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /* USER CODE BEGIN SPI3_MspInit 1 */
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, GPIO_PIN_SET);
  /* USER CODE END SPI3_MspInit 1 */
  }
}

void HAL_SPI_MspDeInit(SPI_HandleTypeDef* spiHandle)
{

  if(spiHandle->Instance==SPI3)
  {
  /* USER CODE BEGIN SPI3_MspDeInit 0 */

  /* USER CODE END SPI3_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_SPI3_CLK_DISABLE();

    /**SPI3 GPIO Configuration
    PC10     ------> SPI3_SCK
    PC11     ------> SPI3_MISO
    PC12     ------> SPI3_MOSI
    */
    HAL_GPIO_DeInit(GPIOC, GPIO_PIN_10|GPIO_PIN_11|GPIO_PIN_12);

  /* USER CODE BEGIN SPI3_MspDeInit 1 */

  /* USER CODE END SPI3_MspDeInit 1 */
  }
}

/* USER CODE BEGIN 1 */
void drv_register_control(void)
{
	uint16_t read = 0;
	uint16_t write = 0;
	uint16_t readvalue = 0;
	//���ƼĴ���1
	write = DRV8303WRITE | REGCTRL1<<11 | GATE_CURRENT | GATE_RESET | PWM_MODE | OCP_MODE | OC_ADJ_SET;
//	write = GATE_CURRENT_0_7_A|GATE_RESET_NOMAL|PWM_MODE_6_INPUTS|OCP_LATCH_SHUT_DOWN|OC_ADJ_SET_14;
//	write = 5472;
	printf("%d\n",write);
	m0_spi_drv8301_read(write);
	read = DRV8303READ | REGCTRL1<<11;
	printf("%x\n",read);
	readvalue = m0_spi_drv8301_read(read);
	printf("%d\n\n",readvalue);
	read = 0;
	write = 0;
	//���ƼĴ���2
	write = DRV8303WRITE | REGCTRL2<<11 | OCTW_MODE | GAIN_AMP_80 | DC_CAL_CH1 | DC_CAL_CH2 | OC_TOFF<<6;
	printf("%d\n",write);
	m0_spi_drv8301_read(write);
	read = DRV8303READ | REGCTRL2<<11;
	printf("%x\n",read);
	readvalue = m0_spi_drv8301_read(read);
	printf("%d\n\n",readvalue);
	//��ȡid
	uint16_t command = 0;
	uint16_t regvalue = 0;
	command = DRV8303READ | REGSTATUS2<<11;
	printf("%x\n",command);
	regvalue = m0_spi_drv8301_read(command);
  regvalue = regvalue;
	printf("%x\n\n",regvalue);
	//���󱨸�
	command = DRV8303READ | REGSTATUS1<<11;
	printf("%x\n",command);
	regvalue = m0_spi_drv8301_read(command);
  regvalue = regvalue;
	printf("%x\n\n",regvalue);
}

void drv_register_control2(void)
{
	uint16_t read = 0;
	uint16_t write = 0;
	uint16_t readvalue = 0;
	//���ƼĴ���1
	write = DRV8303WRITE | REGCTRL1<<11 | GATE_CURRENT | GATE_RESET | PWM_MODE | OCP_MODE | OC_ADJ_SET;
//	write = GATE_CURRENT_0_7_A|GATE_RESET_NOMAL|PWM_MODE_6_INPUTS|OCP_LATCH_SHUT_DOWN|OC_ADJ_SET_14;
//	write = 5472;
	printf("%d\n",write);
	m0_spi_drv8301_read2(write);
	read = DRV8303READ | REGCTRL1<<11;
	printf("%x\n",read);
	readvalue = m0_spi_drv8301_read2(read);
	printf("%d\n\n",readvalue);
	read = 0;
	write = 0;
	//���ƼĴ���2
	write = DRV8303WRITE | REGCTRL2<<11 | OCTW_MODE | GAIN_AMP_80 | DC_CAL_CH1 | DC_CAL_CH2 | OC_TOFF<<6;
	printf("%d\n",write);
	m0_spi_drv8301_read2(write);
	read = DRV8303READ | REGCTRL2<<11;
	printf("%x\n",read);
	readvalue = m0_spi_drv8301_read2(read);
	printf("%d\n\n",readvalue);
	//��ȡid
	uint16_t command = 0;
	uint16_t regvalue = 0;
	command = DRV8303READ | REGSTATUS2<<11;
	printf("%x\n",command);
	regvalue = m0_spi_drv8301_read2(command);
  regvalue = regvalue;
	printf("%x\n\n",regvalue);
	//���󱨸�
	command = DRV8303READ | REGSTATUS1<<11;
	printf("%x\n",command);
	regvalue = m0_spi_drv8301_read2(command);
  regvalue = regvalue;
	printf("%x\n\n",regvalue);
}

uint16_t m0_spi_drv8301_read(uint16_t TxData)
{
	uint16_t data;
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, GPIO_PIN_RESET);
	HAL_Delay(1);
	data = spi3_readwriteword(TxData);
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, GPIO_PIN_SET);
	HAL_Delay(1);
	return data;
}

uint16_t m0_spi_drv8301_read2(uint16_t TxData)
{
	uint16_t data;
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_14, GPIO_PIN_RESET);
	HAL_Delay(1);
	data = spi3_readwriteword(TxData);
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_14, GPIO_PIN_SET);
	HAL_Delay(1);
	return data;
}


uint16_t A5047_Read(void)
{
  uint16_t data;
	
  HAL_GPIO_WritePin(GPIOC,GPIO_PIN_4,GPIO_PIN_RESET);

	data = spi3_readwriteword(0xffff);

	HAL_GPIO_WritePin(GPIOC,GPIO_PIN_4,GPIO_PIN_SET);
	return data&0x3fff ;
}

uint16_t A5047_Read_2(void)
{
  uint16_t data;
	
  HAL_GPIO_WritePin(GPIOB,GPIO_PIN_2,GPIO_PIN_RESET);

	data = spi3_readwriteword(0xffff);

	HAL_GPIO_WritePin(GPIOB,GPIO_PIN_2,GPIO_PIN_SET);
	return data&0x3fff ;
}

uint16_t spi3_readwriteword(uint16_t TxData)
{
	uint16_t rxdata;
	if(HAL_SPI_TransmitReceive(&hspi3,(uint8_t *)&TxData,(uint8_t *)&rxdata,1,1000) !=HAL_OK)
	{
		rxdata=0;
	}
	return rxdata;
}

void pulse_to_THETA_u12(int a,int b,int c,int d,int e,int f,int g,int h,int i,int j,int k,int l,int m,int n,int o,int p,int q,int r,int s,int t)
{
	if(pulse>a && pulse<=b)
	{
		THETA = (float)(pulse-a)/(b-a)*6.2831852f;
	}
	if(pulse>b && pulse<=c)
	{
		THETA = (float)(pulse-b)/(c-b)*6.2831852f;
	}
	if(pulse>c && pulse<=d)
	{
		THETA = (float)(pulse-c)/(d-c)*6.2831852f;
	}
	if(pulse>d && pulse<=e)
	{
		THETA = (float)(pulse-d)/(e-d)*6.2831852f;
	}
	if(pulse>e && pulse<=f)
	{
		THETA = (float)(pulse-e)/(f-e)*6.2831852f;
	}
	if(pulse>f && pulse<=g)
	{
		THETA = (float)(pulse-f)/(g-f)*6.2831852f;
	}
	
	if(pulse>g && pulse<=h)
	{
		THETA = (float)(pulse-g)/(h-g)*6.2831852f;
	}
	if(pulse>h && pulse<=i)
	{
		THETA = (float)(pulse-h)/(i-h)*6.2831852f;
	}
	if(pulse>i && pulse<=j)
	{
		THETA = (float)(pulse-i)/(j-i)*6.2831852f;
	}
	if(pulse>j && pulse<=k)
	{
		THETA = (float)(pulse-j)/(k-j)*6.2831852f;
	}
	if(pulse>k && pulse<=l)
	{
		THETA = (float)(pulse-k)/(l-k)*6.2831852f;
	}
	if(pulse>l && pulse<=m)
	{
		THETA = (float)(pulse-l)/(m-l)*6.2831852f;
	}
	if(pulse>m && pulse<=n)
	{
		THETA = (float)(pulse-m)/(n-m)*6.2831852f;
	}
	if(pulse>n && pulse<=o)
	{
		THETA = (float)(pulse-n)/(o-n)*6.2831852f;
	}
	if(pulse>o && pulse<=p)
	{
		THETA = (float)(pulse-o)/(p-o)*6.2831852f;
	}
	if(pulse>p && pulse<=q)
	{
		THETA = (float)(pulse-p)/(q-p)*6.2831852f;
	}
	if(pulse>q && pulse<=r)
	{
		THETA = (float)(pulse-q)/(r-q)*6.2831852f;
	}
	if(pulse>r && pulse<=s)
	{
		THETA = (float)(pulse-r)/(s-r)*6.2831852f;
	}
	if(pulse>s && pulse<=t)
	{
		THETA = (float)(pulse-s)/(t-s)*6.2831852f;
	}
	
	if(pulse>t && pulse<=16384)
	{
		THETA = (float)(pulse-t)/(16384+a-t)*6.2831852f;
	}
	if(pulse>0 && pulse<=a)
	{
		THETA = (float)(pulse+16384-t)/(16384+a-t)*6.2831852f;
	}
}

void pulse_to_THETA(int a,int b,int c,int d,int e,int f,int g)
{
	if(pulse>a && pulse<=b)
	{
		THETA = -(float)(pulse-a)/(b-a)*6.2831852f;
	}
	if(pulse>b && pulse<=c)
	{
		THETA = -(float)(pulse-b)/(c-b)*6.2831852f;
	}
	if(pulse>c && pulse<=d)
	{
		THETA = -(float)(pulse-c)/(d-c)*6.2831852f;
	}
	if(pulse>d && pulse<=e)
	{
		THETA = -(float)(pulse-d)/(e-d)*6.2831852f;
	}
	if(pulse>e && pulse<=f)
	{
		THETA = -(float)(pulse-e)/(f-e)*6.2831852f;
	}
	if(pulse>f && pulse<=g)
	{
		THETA = -(float)(pulse-f)/(g-f)*6.2831852f;
	}
	if(pulse>g && pulse<=16384)
	{
		THETA = -(float)(pulse-g)/(16384+a-g)*6.2831852f;
	}
	if(pulse>0 && pulse<=a)
	{
		THETA = -(float)(pulse+16384-g)/(16384+a-g)*6.2831852f;
	}
}

void pulse_to_THETA_2(int a,int b,int c,int d,int e,int f,int g)
{
	if(pulse2>a && pulse2<=b)
	{
		THETA2 = -(float)(pulse2-a)/(b-a)*6.2831852f;
	}
	if(pulse2>b && pulse2<=c)
	{
		THETA2 = -(float)(pulse2-b)/(c-b)*6.2831852f;
	}
	if(pulse2>c && pulse2<=d)
	{
		THETA2 = -(float)(pulse2-c)/(d-c)*6.2831852f;
	}
	if(pulse2>d && pulse2<=e)
	{
		THETA2 = -(float)(pulse2-d)/(e-d)*6.2831852f;
	}
	if(pulse2>e && pulse2<=f)
	{
		THETA2 = -(float)(pulse2-e)/(f-e)*6.2831852f;
	}
	if(pulse2>f && pulse2<=g)
	{
		THETA2 = -(float)(pulse2-f)/(g-f)*6.2831852f;
	}
	if(pulse2>g && pulse2<=16384)
	{
		THETA2 = -(float)(pulse2-g)/(16384+a-g)*6.2831852f;
	}
	if(pulse2>0 && pulse2<=a)
	{
		THETA2 = -(float)(pulse2+16384-g)/(16384+a-g)*6.2831852f;
	}
}

/* USER CODE END 1 */
