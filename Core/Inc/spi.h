/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file    spi.h
  * @brief   This file contains all the function prototypes for
  *          the spi.c file
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __SPI_H__
#define __SPI_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

extern SPI_HandleTypeDef hspi3;

/* USER CODE BEGIN Private defines */
	
	
	
//���ƼĴ���
#define GATE_CURRENT  0x0002
#define GATE_RESET    0x0000
#define PWM_MODE			0x0000
#define OCP_MODE      0x0000
#define OC_ADJ_SET    (17<<6)
#define OCTW_MODE     0x0000
#define GAINVALUE     0x0004
#define DC_CAL_CH1    0x0000
#define DC_CAL_CH2    0x0000
#define OC_TOFF       0x0000
//��дָ��
#define DRV8303WRITE  (0x00<<15)
#define DRV8303READ   (0x01<<15)
//״̬�Ĵ�����ַ
#define REGSTATUS1    0x00
#define REGSTATUS2    0x01
//���ƼĴ�����ַ
#define REGCTRL1      0x02
#define REGCTRL2      0x03

/**********************������������*************************/

#define DRV8301_REG0    0
#define DRV8301_REG1    1
#define DRV8301_REG2    2
#define DRV8301_REG3    3

//reg2
#define GATE_CURRENT_1_7_A        0x0000
#define GATE_CURRENT_0_7_A        0x0001
#define GATE_CURRENT_0_25_A       0x0002

#define GATE_RESET_NOMAL          0x0000
#define GATE_RESET_FAULTS         0x0004

#define PWM_MODE_6_INPUTS         0x0000
#define PWM_MODE_3_INPUTS         0x0008

#define OCP_CURR_LIMIT            0x0000
#define OCP_LATCH_SHUT_DOWN       0x0010
#define OCP_REPORT_ONLY           0x0020
#define OCP_DISABLED              0x0030

#define OC_ADJ_SET_0              0x0000      //0.060V
#define OC_ADJ_SET_1              0x0040      //0.068V
#define OC_ADJ_SET_2              0x0080      //0.076V
#define OC_ADJ_SET_3              0x00c0      //0.086V
#define OC_ADJ_SET_4              0x0100      //0.097V
#define OC_ADJ_SET_5              0x0140      //0.109V
#define OC_ADJ_SET_6              0x0180      //0.123V
#define OC_ADJ_SET_7              0x01c0      //0.138V
#define OC_ADJ_SET_8              0x0200      //0.155V
#define OC_ADJ_SET_9              0x0240      //0.175V
#define OC_ADJ_SET_10             0x0280      //0.197V
#define OC_ADJ_SET_11             0x02c0      //0.222V
#define OC_ADJ_SET_12             0x0300      //0.250V
#define OC_ADJ_SET_13             0x0340      //0.282V
#define OC_ADJ_SET_14             0x0380      //0.317V
#define OC_ADJ_SET_15             0x03c0      //0.358V
#define OC_ADJ_SET_16             0x0400      //0.403V
#define OC_ADJ_SET_17             0x0440      //0.454V
#define OC_ADJ_SET_18             0x0480      //0.511V
#define OC_ADJ_SET_19             0x04c0      //0.576V
#define OC_ADJ_SET_20             0x0500      //0.648V
#define OC_ADJ_SET_21             0x0540      //0.730V
#define OC_ADJ_SET_22             0x0580      //0.822V
#define OC_ADJ_SET_23             0x05c0      //0.926V
#define OC_ADJ_SET_24             0x0600      //1.043V
#define OC_ADJ_SET_25             0x0640      //1.175V
#define OC_ADJ_SET_26             0x0680      //1.324V
#define OC_ADJ_SET_27             0x06c0      //1.491V
#define OC_ADJ_SET_28             0x0700      //1.679V
#define OC_ADJ_SET_29             0x0740      //1.892V
#define OC_ADJ_SET_30             0x0780      //2.131V
#define OC_ADJ_SET_31             0x07c0      //2.400V

//reg3
#define OCTW_OT_AND_OC            0x0000
#define OCTW_OT_ONLY              0x0001
#define OCTW_OC_ONLY              0x0002

#define GAIN_AMP_10               0x0000              
#define GAIN_AMP_20               0x0004
#define GAIN_AMP_40               0x0008
#define GAIN_AMP_80               0x000C

#define DC_CAL_CH1_CON            0x0000
#define DC_CAL_CH1_DIS            0x0010

#define DC_CAL_CH2_CON            0x0000
#define DC_CAL_CH2_DIS            0x0020

#define OC_TOFF_CYCLE             0x0000
#define OC_TOFF_OFF_TIME          0x0040

/***********************************************************/

extern float angle;
extern float e_angle;
extern int lastpulse,deltapulse;
extern long long totalpulse;
extern long long lasttotalpulse;
extern long long speed_totalpulse;
extern float speed_rpm,speed_rps;

extern float angle2;
extern float e_angle2;
extern int lastpulse2,deltapulse2;
extern long long totalpulse2;
extern long long lasttotalpulse2;
extern long long speed_totalpulse2;
extern float speed_rpm2,speed_rps2;

uint16_t spi3_readwriteword(uint16_t TxData);
uint16_t m0_spi_drv8301_read(uint16_t TxData);
uint16_t m0_spi_drv8301_read2(uint16_t TxData);
uint16_t A5047_Read(void);
uint16_t A5047_Read_2(void);
void pulse_to_THETA(int a,int b,int c,int d,int e,int f,int g);
void pulse_to_THETA_2(int a,int b,int c,int d,int e,int f,int g);
void pulse_to_THETA_u12(int a,int b,int c,int d,int e,int f,int g,int h,int i,int j,int k,int l,int m,int n,int o,int p,int q,int r,int s,int t);
void drv_register_control(void);
void drv_register_control2(void);
/* USER CODE END Private defines */

void MX_SPI3_Init(void);

/* USER CODE BEGIN Prototypes */

/* USER CODE END Prototypes */

#ifdef __cplusplus
}
#endif

#endif /* __SPI_H__ */

